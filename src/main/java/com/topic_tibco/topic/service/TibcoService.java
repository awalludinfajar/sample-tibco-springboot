package com.topic_tibco.topic.service;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import com.topic_tibco.topic.config.QueueConfiguration;
import com.topic_tibco.topic.config.TopicConfiguration;

@Service
public class TibcoService {

    @Autowired
    private TopicConfiguration topicConfiguration;

    @Autowired
    private QueueConfiguration queueConfiguration;

    public void sendTopic(String msg) throws JMSException {
        TextMessage message = topicConfiguration.sessionFactory().createTextMessage(msg);
        topicConfiguration.producer().send(message);
    }

    public void sendQueue(String msg) throws JMSException {
        TextMessage message = queueConfiguration.sessionFactory().createTextMessage(msg);
        queueConfiguration.setup().send(message);
    }
}
