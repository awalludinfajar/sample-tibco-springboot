package com.topic_tibco.topic.controller;

import javax.jms.JMSException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.topic_tibco.topic.model.SendMessage;
import com.topic_tibco.topic.service.TibcoService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
public class SampleTopicController {
    
    @Autowired
    private TibcoService tibcoService;

    // I m updated for testing docker build image in gitlab
    @PostMapping(path = "/api/send/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public String produceMessageTopic(@RequestBody SendMessage message) {
        try {
            tibcoService.sendTopic(message.topicMessage);
            tibcoService.sendQueue(message.queueMessage);
            return String.format("{\"message\":\"%s\"}", message.topicMessage);
        } catch (JMSException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}
